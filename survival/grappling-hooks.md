# Grappling Hooks

#### There are multiple tiers of grappling hooks with different amount of usages

![50 Uses](../Survival/image.png) ![100 Uses](../Survival/image-1.png) ![200 Uses](../Survival/image-2.png) ![250 Uses](../Survival/image-3.png) ![400 Uses](../Survival/image-4.png) ![1000 Uses](../Survival/image-5.png)
