# Lifesteal

#### Lose all your hearts, and you will be banned off the server

#### Players start with 10 hearts, and can have a maximum of 20 hearts

#### `/lswithdraw` to withdraw a heart and turn it into an item

#### `/lsrecipe` for the recipe of lifesteal hearts

#### Heart crafting recipe:

right clicking with the lifesteal heart in your hand will give you an extra heart

![Lifesteal Heart](../Survival/image-6.png)
